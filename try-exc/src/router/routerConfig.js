import { lazy } from 'react'

const routes = [
    {
        path: "/home",
        component: lazy(() => import("../views/Home/Home.js")),
    },
    {
        path: "/my",
        component: lazy(() => import("../views/My/My.js")),
    },
    {
        from: "/",
        to: "/home"
    }
]
export default routes