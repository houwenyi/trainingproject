import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom'
import { Suspense } from 'react'
function RouterViews({ routes }) {
    // console.log(routes);
    return <Suspense fallback={<div>Loading...</div>}>
       
        <Switch >
            {
                routes.map(item => {
                    if (item.to) {
                        if (item.from) {
                            return <Redirect to={item.to} from={item.from} key={item.to} exact></Redirect>
                        }
                        return <Redirect to={item.to} key={item.to} exact></Redirect>
                    }
                    return <Route
                        key={item.path}
                        path={item.path}
                        render={routerProps => {
                            if (item.children) {
                                return <item.component {...routerProps} routes={item.children}></item.component>
                            } else {
                                return <item.component {...routerProps}></item.component>
                            }
                        }}
                    ></Route>
                })
            }
        </Switch>
    </Suspense>
}
function Router({ routes }) {
    return <BrowserRouter>
        <RouterViews routes={routes} />
    </BrowserRouter>

}
export default Router