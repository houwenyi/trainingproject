import React, { Component } from 'react';
import RouterViews from "./router/RouterViews"
import config from "./router/routerConfig"
class App extends Component {
  render() {
    return (
      <div>
          <RouterViews routes={config}></RouterViews>
      </div>
    );
  }
}

export default App;